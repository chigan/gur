# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from rk1.views import main
from rk1.views import za
from rk1.views import ra
from rk1.views import ha

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hackgur.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', main),
    url(r'^za$',za),
    url(r'^ra$',ra),
    url(r'^ha$',ha),
)
