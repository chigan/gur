# -*- coding: utf-8 -*-
import string
import  math
import itertools
from fractions import gcd

alphaavit = 'АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЫЬЮЯ'
LENGTH_ALPHAVIT = len(alphaavit)
STAT_ALPHAVIT = 'ОАЕИНТРСЛВКПМУДЯЫЬЗБГЙЧЮХЖШЦЩФЭЪ'

def reinput(s=""):
    s = s.replace('Э','Е')
    s = s.replace('Ё','Е')
    s = s.replace('Й','И')
    s = s.replace('Ъ','Ь')
    return s

def del_spaces(s):

    if all((x not in set(alphaavit+' ') for x in s)):
        print('Ошибка ввода_1')
        exit()

    spaces = tuple(i for i in range(len(s)) if s[i]==' ')
    s = s.replace(' ','')
    return (s,spaces)

def my_ord(s):
    return alphaavit.find(s)

def my_chr(i):
    return alphaavit[i]

def add_spaces(s,spaces):
    s = list(s)
    for i in spaces: s.insert(i,' ')
    s = ''.join(s)
    return s

def chifrat(s, key, unch=False, la=29, debug=False):
    (s, spases) = del_spaces(s)

    unch = unch and -1 or 1

    result = ''

    for i,ch in enumerate(s):
        n_ch = my_ord(ch) + unch*my_ord(key[i % len(key)])
        result += my_chr(n_ch % LENGTH_ALPHAVIT)

        if debug:
            print( \
                '| {0} | {1:<2} | {2} '.format(ch,my_ord(ch),((unch > 0) and '+'  or '-')) + \
                '| {0} | {1:<2} | = '.format(key[i%len(key)],my_ord(key[i%len(key)])) + \
                '| {0} | {1:<2} |'.format(result[-1],my_ord(result[-1])) \
                )

    result = add_spaces(result,spases)
    return result

def hack_length_keyword(s='', debug=False):
    if not s.isupper():
        print('Ошибка ввода_2')
        exit()
    s,spaces=del_spaces(s)
    #наполняем лист статистики фрагментами
    stat_list = []
    for l in range(2,len(s)//2):
        stat = {}
        for b in range(len(s)-l+1):
            buf_s = s[b:b+l]
            stat[buf_s] = stat.get(buf_s,0)+1
        stat = stat.items()
        stat = sorted(stat, key = lambda x: x[1], reverse=True)
        stat_list.append(stat)

    #удалим фрагменты, которые вошли только 1 раз
    i=0
    while i < len(stat_list):
        j = 0
        while j < len(stat_list[i]):
            if stat_list[i][j][1] == 1:
                del stat_list[i][j]
            else:
                j+=1
        if not stat_list[i]:
            del stat_list[i]
        else:
            i+=1
    print(stat_list)
    #создание словаря расстояний между фрагментами
    nods = []
    for stat in stat_list:
        for f_v in stat:
            ivalue = s.find(f_v[0])
            for _ in range(f_v[1]-1):
                new_ivalue = s.find(f_v[0], ivalue+1)
                nods.append(new_ivalue-ivalue)
                if debug: print('фрагмент {0} повторяется {1} раз, начало {2} конец {3} расстояние {4}'.format(f_v[0], f_v[1], ivalue, new_ivalue, new_ivalue-ivalue))
                ivalue=new_ivalue+1
    nods=list(set(nods))

    #поиск нод
    set_nods = set()
    #декартово произведение всех расстояний для поиска всех НОД
    for element in itertools.product(nods,nods):
        if element[0] != element[1]:
            set_nods.add(frozenset(element))

    set_nods = list(set_nods)
    for i in range(len(set_nods)):
        set_nods[i]=tuple(set_nods[i])
    #подсчёт всех нод
    dict_nods = {}
    for element in set_nods:
        g = gcd(*element)
        if not g in {1, 2}:
            dict_nods[g] = dict_nods.get(g,0) + 1

    dict_nods = dict_nods.items()
    dict_nods = sorted(dict_nods, key = lambda x: x[1], reverse=True)
    if debug: print(dict_nods)
    return [x[0] for x in dict_nods]

def hack_text(s='',key=0):
    s,spaces=del_spaces(s)
    ch_table=[]
    for i in range(key):
        ch_table.append(s[i::key])

    s=add_spaces(s,spaces)
    ch_deckt_list = []
    for ch_list in ch_table:
        ch_deckt = {}
        for ch in ch_list:
            ch_deckt[ch] = ch_deckt.get(ch,0) + 1
        ch_deckt = ch_deckt.items()
        ch_deckt = sorted(ch_deckt,key = lambda x: x[1],reverse = True)
        #print(ch_deckt)
        ch_deckt_list.append(ch_deckt)
#Теперь в ch_deckt - отсортированные по встречаемости буквы
    for i in range(len(ch_deckt_list)):
        j = 0
        while j < len(ch_deckt_list[i]):
            if ch_deckt_list[i][j][1] == 1:
                del ch_deckt_list[i][j]
            else:
                j+=1

    possible_keys = []
    for ch_list in ch_deckt_list:
        #print(ch_list)
        possible_ch = []
        for i in range(len(ch_list)):
            possible_ch.append(my_chr(((my_ord(ch_list[i][0])+LENGTH_ALPHAVIT-my_ord(STAT_ALPHAVIT[i]))%LENGTH_ALPHAVIT)))
        possible_keys.append(possible_ch)

    return possible_keys

STAT_ALPHAVIT = reinput(STAT_ALPHAVIT)
