
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from urlparse import urlparse, parse_qs
import re

import string
import  math
import itertools
from fractions import gcd
import re

alphaavit = u'АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЫЬЮЯ'
LENGTH_ALPHAVIT = len(alphaavit)
STAT_ALPHAVIT = u'ОАЕИНТРСЛВКПМУДЯЫЬЗБГЙЧЮХЖШЦЩФЭЪ'

def reinput(s=""):
    s = s.replace(u'Э',u'Е')
    s = s.replace(u'Ё',u'Е')
    s = s.replace(u'Й',u'И')
    s = s.replace(u'Ъ',u'Ь')
    s = re.sub("^\s+|\n|\r|\s+$", ' ', s)

    return s

def del_spaces(s):

    if all((x not in set(alphaavit+' ') for x in s)):
        print('Ошибка ввода_1')
        exit()

    spaces = tuple(i for i in range(len(s)) if s[i]==' ')
    s = s.replace(' ','')
    return (s,spaces)

def my_ord(s):
    return alphaavit.find(s)

def my_chr(i):
    return alphaavit[i]

def add_spaces(s,spaces):
    s = list(s)
    for i in spaces: s.insert(i,' ')
    s = ''.join(s)
    return s

def chifrate(s, key, unch=False, la=29, debug=False):
    (s, spases) = del_spaces(s)

    unch = unch and -1 or 1

    result = ''

    for i,ch in enumerate(s):
        n_ch = my_ord(ch) + unch*my_ord(key[i % len(key)])
        result += my_chr(n_ch % LENGTH_ALPHAVIT)

        if debug:
            print( \
                '| {0} | {1:<2} | {2} '.format(ch,my_ord(ch),((unch > 0) and '+'  or '-')) + \
                '| {0} | {1:<2} | = '.format(key[i%len(key)],my_ord(key[i%len(key)])) + \
                '| {0} | {1:<2} |'.format(result[-1],my_ord(result[-1])) \
                )

    result = add_spaces(result,spases)
    return result

def hack_length_keyword(s='', debug=False):
    if not s.isupper():
        print('Ошибка ввода_2')
        exit()
    s,spaces=del_spaces(s)
    debug_list = ''
    #наполняем лист статистики фрагментами
    stat_list = []
    for l in range(2,len(s)//2):
        stat = {}
        for b in range(len(s)-l+1):
            buf_s = s[b:b+l]
            stat[buf_s] = stat.get(buf_s,0)+1
        stat = stat.items()
        stat = sorted(stat, key = lambda x: x[1], reverse=True)
        stat_list.append(stat)

    #удалим фрагменты, которые вошли только 1 раз
    i=0
    while i < len(stat_list):
        j = 0
        while j < len(stat_list[i]):
            if stat_list[i][j][1] == 1:
                del stat_list[i][j]
            else:
                j+=1
        if not stat_list[i]:
            del stat_list[i]
        else:
            i+=1
    print(stat_list)
    #создание словаря расстояний между фрагментами
    nods = []
    for stat in stat_list:
        for f_v in stat:
            if debug:
                debug_list +='Фрагмент {0} повторяется {1} раз'.format(f_v[0],f_v[1]) + \
                '\n________________________\n|Начало|Конец|Расстояние|\n'
            ivalue = s.find(f_v[0])
            for _ in range(f_v[1]-1):
                new_ivalue = s.find(f_v[0], ivalue+1)
                nods.append(new_ivalue-ivalue)
                if debug:
                    debug_list += '|{0:<6}|{1:<5}|{2:<10}|\n'.format(ivalue, new_ivalue, new_ivalue-ivalue)
                ivalue=new_ivalue
            if debug: debug_list += '|_______________________|\n\n'
    nods=list(set(nods))

    #поиск нод
    set_nods = set()
    #декартово произведение всех расстояний для поиска всех НОД
    for element in itertools.product(nods,nods):
        if element[0] != element[1]:
            set_nods.add(frozenset(element))

    set_nods = list(set_nods)
    for i in range(len(set_nods)):
        set_nods[i]=tuple(set_nods[i])
    #подсчёт всех нод
    dict_nods = {}
    for element in set_nods:
        g = gcd(*element)
        if not g in {1, 2}:
            dict_nods[g] = dict_nods.get(g,0) + 1

    dict_nods = dict_nods.items()
    dict_nods = sorted(dict_nods, key = lambda x: x[1], reverse=True)
    if debug:
        for n in dict_nods:
            debug_list += 'для {0} пар НОД={1}\n'.format(n[1],n[0])
    return ([x[0] for x in dict_nods], debug_list)


def hack_text(s='',key=0):
    s,spaces=del_spaces(s)
    ch_table=[]
    for i in range(key):
        ch_table.append(s[i::key])

    s=add_spaces(s,spaces)
    ch_deckt_list = []
    for ch_list in ch_table:
        ch_deckt = {}
        for ch in ch_list:
            ch_deckt[ch] = ch_deckt.get(ch,0) + 1
        ch_deckt = ch_deckt.items()
        ch_deckt = sorted(ch_deckt,key = lambda x: x[1],reverse = True)
        #print(ch_deckt)
        ch_deckt_list.append(ch_deckt)
#Теперь в ch_deckt - отсортированные по встречаемости буквы
    for i in range(len(ch_deckt_list)):
        j = 0
        while j < len(ch_deckt_list[i]):
            if ch_deckt_list[i][j][1] == 1:
                del ch_deckt_list[i][j]
            else:
                j+=1

    possible_keys = []
    for ch_list in ch_deckt_list:
    	debage_list = ''
        possible_ch = []
        debage_list+='{0} встретилась {1} раз\n'.format(*ch_list[0])
        for i in range(11):
            possible_ch.append((my_chr(((my_ord(ch_list[0][0])+LENGTH_ALPHAVIT-my_ord(STAT_ALPHAVIT[i]))%LENGTH_ALPHAVIT))))
            debage_list+='({0}|{1:<2})+'.format(STAT_ALPHAVIT[i],my_ord(STAT_ALPHAVIT[i]))+ \
                  '({0}|{1:<2})='.format(possible_ch[-1],my_ord(possible_ch[-1]))+ \
                  '({0}|{1:<2})\n'.format(ch_list[0][0],my_ord(ch_list[0][0]))

        possible_keys.append((possible_ch,debage_list))

    return possible_keys

STAT_ALPHAVIT = reinput(STAT_ALPHAVIT)

def main(request):
	
	return render(request, 'index.html',{'text':'СЮДА ТЕКСТ', 'key':'СЮДА КЛЮЧ'} )

def za(request):
	text = (request.GET[u'text']).upper()	
	text = reinput(text)
	key = (request.GET[u'key']).upper()

	if not (text and key):
		return HttpResponseRedirect('/')

	result = chifrate(text,key)

	context = {'text':text,'key':key,'result':result}
	return render(request, 'index.html',context )

def  ra(request):
	text = (request.GET[u'text']).upper()	
	text = reinput(text)
	key = (request.GET[u'key']).upper()

	if not (text and key):
		return HttpResponseRedirect('/')

	result = chifrate(text,key,unch=True)

	context = {'text':text,'key':key, 'result':result}
	return render(request, 'index.html',context )

def  ha(request):
	text = (request.GET[u'text']).upper()	
	text = reinput(text)
	key = (request.GET[u'key']).upper()

	if not (text):
		return HttpResponseRedirect('/')

	length_l = hack_length_keyword(text,debug=True)
	posob_keys = []
	for l in length_l[0][:5]:
		posob_keys.append((l, hack_text(text,l)))
	print posob_keys
	context = {'text':text, 'length_debug':length_l[1], 'key':key, 'result': posob_keys}
	return render(request, 'index_for_hack.html', context)